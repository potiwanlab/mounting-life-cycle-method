
import './App.css';
import { Component } from "react";

export default class App extends Component {
  state = {
    lists: [],
    loading: false,
    show: false,
    detail: "",
    todo: ""
  };

  handleCloseDetail = () => {
    this.setState({
      show: false
    });
    alert("Todo: " + this.state.todo)
  };
  
  async componentDidMount() {
    this.setState({
      loading: true
    })
    const response = await fetch('https://todo.showkhun.co/lists')
    const data = await response.json()
    this.setState({
      lists: data.lists,
      loading: false
    })
  }

  handleClick = (todo) => () => {
    this.setState({
      detail: todo.detail,
      todo: todo.todo,
      show: true
    })
    console.log(this.state.detail)
  }
  
  render() {
    return (
      <div className="App">
        <h1>DetailView</h1>
        {this.state.loading ? (
          <h1>Loading...</h1>
        ) : (
          false
        )}
        <ul>
          {
            this.state.lists.map((todo) =>
              <li key={todo.id}><a href="#" onClick={this.handleClick(todo)}>{todo.todo}</a></li>)
          }
        </ul>
        {this.state.show ? (
          <Detail
            todo={this.state.todo}
            detail={this.state.detail}
            show={this.state.show}
            loading={this.state.loading}
            onClose={this.handleCloseDetail}
          />
        ) : (
          false
        )}
      </div>
    );
  }
}

export class Detail extends Component {
  render() {
    return (
      <div
        style={{
          background: "#64b4e2",
          zIndex: 999,
          width: "20%",
          margin: "0 auto",
          textAlign: "left",
          height: 100,
          display: "block",
          padding: 10,
          position: "relative"
        }}
      >
        <button
          style={{
            position: "absolute",
            top: 10,
            left: "auto",
            right: 10
          }}
          onClick={this.props.onClose}
        >
          x
        </button>

        <strong>Detail</strong>
        <p>Todo : {this.props.todo}</p>
        <p>Detail : {this.props.detail}</p>
      </div>
    );
  }
}

