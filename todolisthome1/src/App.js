import { Component } from "react";
import "./styles.css";

export default class App extends Component {
  state = {
    lists: [],
    loading: false
  };

  async componentDidMount() {
    this.setState({
      loading: true
    })
    const res = await fetch('https://todo.showkhun.co/lists')
    const data = await res.json()
    this.setState({
      lists: data.lists,
      loading: false
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Hello TODOLIST</h1>
        {this.state.loading ? (
          <h1>Loading.....</h1>
        ) : (
          false
        )}
        <ul>
          {
            this.state.lists.map((todo) =>
            <li key={todo.id}>
              <p> ID: {todo.id} <br></br>
                  Name: {todo.todo} <br></br>
                  Detail: {todo.detail} <br></br>
                  Status: {todo.status} <br></br>
              </p>
            </li>
            )
          }
        </ul>
      </div>
    );
  }
}
